FROM ruby:2.7.4

# Danger manipulates git diff output. When a unicode char is present in the diff, it chokes with
# (Danger::DSLError) [!] Invalid `Dangerfile` file: invalid byte sequence in US-ASCII
# eg. https://gitlab.com/gitlab-org/gitlab-foss/-/jobs/275286625
ENV LC_ALL "C.UTF-8"

COPY . /danger/

WORKDIR /danger/

RUN apt-get update \
    && apt-get install -y git \
    && git version \
    && apt-get autoremove -yq \
    && apt-get clean -yqq \
    && rm -rf /var/lib/apt/lists/* \
    && bundle config set path /danger/vendor/ \
    && bundle install

WORKDIR /
