require 'test/unit'
require_relative 'changelog'

class TestChangelog < Test::Unit::TestCase
  def test_changelog_first_entry_line_with_no_lines
    assert changelog_first_entry_line([]).nil?
  end

  def test_changelog_first_entry_line_with_no_space_after_version
    corpus = <<~'CL'
      # Analyzer changelog

      ## v2.15.0
      - Update to analyzer v2.31.2 (!56)
        + Update benefit 1
        + Update benefit 2

      ## v2.14.0
      - Such a feature! (!53)
    CL
    expected = "- Update to analyzer v2.31.2 (!56)\n"
    assert_equal expected, changelog_first_entry_line(corpus.lines)
  end

  def test_keepachangelog_first_entry_line
    corpus = <<~'CL'
      # Changelog
      All notable changes to this project will be documented in this file.

      The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
      and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

      The release dates can be found on the [releases page](...).

      ## v1.1.0
      ### Added

      - Add `awesomesauce` functionality for generically parsing rules (!5)

      ## v1.0.0
      ### Added

      - Add rules package for custom rules support (!1)
    CL
    expected = "- Add `awesomesauce` functionality for generically parsing rules (!5)\n"
    assert_equal expected, changelog_first_entry_line(corpus.lines)
  end

  def test_parse_changelog_mr_iid_with_no_iid
    assert parse_changelog_mr_iid("- This is an awesome change with no MR id").nil?
  end

  def test_parse_changelog_mr_iid_with_iid
    assert_equal "23", parse_changelog_mr_iid("- This is an awesome change (!23)")
  end

  def test_parse_changelog_mr_iid_with_iid_a_contributor
    assert_equal "23", parse_changelog_mr_iid("- This is an awesome change (!23 @F00)")
  end

  def test_parse_changelog_mr_iid_with_iid_contributors
    assert_equal "23", parse_changelog_mr_iid("- This is an awesome change (!23 @F00 @baz-bar)")
  end

  def test_needs_changelog
    assert_true needs_changelog?(["type::feature", "feature::addition"])
    assert_true needs_changelog?(["type::feature", "feature::enhancement"])
    assert_true needs_changelog?(["type::bug"])
    assert_false needs_changelog?(["documentation", "doc-only"])
  end

  def test_labels_list
    assert_equal '~"backend", ~"type::feature", ~"feature::addition"', labels_list(["backend", "type::feature", "feature::addition"])
  end

  def test_commit_trailer
    commit_message_with_only_trailer = <<~'EOF'
      Update git vendor to gitlab

      Now that we are using gitaly to compile git, the git version isn't known
      from the manifest, instead we are getting the gitaly version. Update our
      vendor field to be `gitlab` to avoid cve matching old versions.

      Changelog: changed
    EOF
    assert_equal "changed", changelog_trailer(commit_message_with_only_trailer)

    commit_message_with_trailer_and_mr = <<~'EOF'
      Update git vendor to gitlab

      Now that we are using gitaly to compile git, the git version isn't known
      from the manifest, instead we are getting the gitaly version. Update our
      vendor field to be `gitlab` to avoid cve matching old versions.

      Changelog: changed
      MR: https://gitlab.com/foo/bar/-/merge_requests/123
    EOF
    assert_equal "changed", changelog_trailer(commit_message_with_trailer_and_mr)

    commit_message_with_trailer_mr_and_ee = <<~'EOF'
      Update git vendor to gitlab

      Now that we are using gitaly to compile git, the git version isn't known
      from the manifest, instead we are getting the gitaly version. Update our
      vendor field to be `gitlab` to avoid cve matching old versions.

      Changelog: changed
      MR: https://gitlab.com/foo/bar/-/merge_requests/123
      EE: true
    EOF
    assert_equal "changed", changelog_trailer(commit_message_with_trailer_mr_and_ee)

    commit_message_with_no_trailer = <<~EOF
      Update git vendor to gitlab

      Now that we are using gitaly to compile git, the git version isn't known
      from the manifest, instead we are getting the gitaly version. Update our
      vendor field to be `gitlab` to avoid cve matching old versions.
    EOF
    assert_nil changelog_trailer(commit_message_with_no_trailer)

    matching_string_in_wrong_place = "Updated changelog: added more entries"
    assert_nil changelog_trailer(matching_string_in_wrong_place)

    security_change = <<~EOF
      Fix security bug

      Changelog: security
    EOF
    assert_equal "security", changelog_trailer(security_change)
  end
end
