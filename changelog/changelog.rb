NO_CHANGELOG_LABELS = [
  'documentation'
].freeze

TRAILER_PATTERN = /^Changelog: (added|fixed|changed|deprecated|removed|security|performance|other)/im

def changelog_first_entry_line(changelog_lines)
  mr_line = changelog_lines.find_index { |line| line.start_with?("## v") }
  if mr_line
    first_line = changelog_lines[mr_line+1]
    return first_line unless first_line.start_with?("### ")
    return changelog_lines[mr_line+3]
  end
end

def parse_changelog_mr_iid(entry_line)
  return if entry_line.nil?
  matches = entry_line.match(/\s\(!(\d{1,}) *[^\)]*\)$/)
  matches && matches[1]
end

def needs_changelog?(labels)
  (labels & NO_CHANGELOG_LABELS).empty?
end

def labels_list(labels, sep: ', ')
  labels.map { |label| %Q{~"#{label}"} }.join(sep)
end

def changelog_trailer(commit_message)
  match = commit_message.match(TRAILER_PATTERN)
  return if match.nil?
  match.captures.first
end
