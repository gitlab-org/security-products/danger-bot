PROJECTS_DATA = "https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/projects.yml".freeze

require 'httparty'

class ProjectHelper
  def projects
    puts "Fetching project data from #{PROJECTS_DATA}"
    yaml = with_retries { HTTParty.get(PROJECTS_DATA, format: :plain) }
    YAML.load(yaml)
  end

  def current_project
    url = ENV["CI_PROJECT_URL"]
    return '' if url.empty?
    projects.each do |key, data|
      return key if data["link"] == url
    end
    ''
  end

  def with_retries(attempts: 3)
    yield
  rescue Errno::ECONNRESET, OpenSSL::SSL::SSLError, Net::OpenTimeout
    retry if (attempts -= 1) > 0
    raise
  end
end
