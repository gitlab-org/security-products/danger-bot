# danger-bot

`danger-bot` is the Sec section's configuration for running [danger](https://github.com/danger/danger)
CI jobs against our projects. See [danger documentation](https://docs.gitlab.com/ee/development/dangerbot.html) for generic usage across GitLab.

This project should not be used directly but included in target projects
via our shared [ci-templates](https://gitlab.com/gitlab-org/security-products/ci-templates/).

## Release process

Our versioning does not currently follow a versioning schema but a simple PATCH incrementation per release.

1. Create a `git tag` incrementing the version number
1. Update the CI configuration with the corresponding `DANGER_BOT_VERSION`, such as the default [CI template](https://gitlab.com/gitlab-org/security-products/ci-templates/-/blob/master/includes-dev/go.yml)